class CreateGames < ActiveRecord::Migration[5.2]
  def change
    create_table :games do |t|
      t.string :name
      t.string :genre
      t.text :description
      t.date :release
      t.references :developer, foreign_key: true

      t.timestamps
    end
  end
end
