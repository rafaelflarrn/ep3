class AddAttachmentAvatarToDevelopers < ActiveRecord::Migration[5.2]
  def self.up
    change_table :developers do |t|
      t.attachment :avatar
    end
  end

  def self.down
    remove_attachment :developers, :avatar
  end
end
