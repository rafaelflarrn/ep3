json.extract! game, :id, :name, :genre, :description, :release, :developer_id, :created_at, :updated_at
json.url game_url(game, format: :json)
