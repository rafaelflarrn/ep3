class Game < ApplicationRecord
  belongs_to :developer

  validates :name, :genre, :description, :release , presence: true

  has_attached_file :avatar, styles: { medium: "300x300>", thumb: "50x50>" }
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\z/


end
